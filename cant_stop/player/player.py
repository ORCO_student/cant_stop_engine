# abstract class of player
class Player():
    def __init__(self):
        """
        Constructor.
        """
        self.name = "abstract"
        self.game = None

    def choose_move(self, dice_roll, choices):
        """
        Chooses one move among the possible move choices
        Args:
            dice_roll: tuple of 4 int, used for human player output
            choices: dict(pair: [move]) where:
                pair is a tuple of 2 ints
                [move] is a list of moves
                move is a dict(column: ACTION) where:
                    c is an int of COLUMNS
                    ACTION is a str of {MOVE, ADD, ....}
        Returns:
            move: the chosen move
                dict(column: ACTION) where:
                    c is an int of COLUMNS
                    ACTION is a str of {MOVE, ADD, ....}
        """
        pass

    def build_an_anchor(self):
        """
        Decides to build an anchor ot not.
        Returns:
            True if player wishes to build an anchor
            False if player wishes to throw the dices
        """
        pass

    def burst(self, dice_roll, dice_pairs):
        """
        Tells the player that no moves are possible with the last roll
        Args:
            roll: a tuple of 4 ints, the dice throw result
        """
        pass
