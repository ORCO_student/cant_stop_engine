from cant_stop.player.player import Player


class HumanPlayer(Player):
    def __init__(self, p_name='anonymous'):
        super().__init__()
        self.name = p_name

    def choose_move(self, dice_roll, choices):
        self.game.print_ladders(self)
        print("")
        self.print_dice_results(dice_roll, [pair for pair in choices])
        self.print_neutral_markers()
        print("\n** Possible moves:")
        choice_id = 1
        choice_list = []
        for pair in choices:
            a, b = pair
            print("With %s and %s:" % (a, b))
            if len(choices[pair]) == 0:
                print("   no possible move")
            else:
                self.print_moves(choices[pair], choice_id)
                for possible_move in choices[pair]:

                        choice_list += [possible_move]
                        choice_id += 1
        while True:
            try:
                player_choice = int(input(
                    "please choose a move (%s -> %s)\n" % (1, len(choice_list))
                ))
            except Exception as _:
                player_choice = 0
            if 1 <= player_choice <= len(choice_list):
                break
        return choice_list[player_choice-1]

    def build_an_anchor(self):
        return input('Do you wish to play again ? [Y/n]') not in ['Y', 'y', '']

    def burst(self, dice_roll, dice_pairs):
        self.print_neutral_markers()
        self.print_dice_results(dice_roll, dice_pairs)
        print("    ****    BURST ! %s has fallen back to:    ****" % self.name)
        self.game.print_ladders(self)

    def print_neutral_markers(self):
        markers_line = "Neutral markers: "
        if self.game.unused_markers == 3:
            markers_line = "** All 3 markers available."
        else:
            markers_line = "** markers on column %s. %s markers available." % (
                ', '.join([str(c) for c in self.game.markers]),
                self.game.unused_markers
            )
        print(markers_line)

    def print_dice_results(self, dice_roll, dice_pairs):
        print("** Dice result : %s " % ', '.join([str(d) for d in dice_roll]))
        print("** Giving the following pair(s): %s" % dice_pairs)

    def print_moves(self, moves, move_id):
        for move in moves:
            print("choice %s: " % move_id + "; ". join([
                "column %s: %s" % (col, act) for col, act in move.items()
            ]))
            move_id += 1
