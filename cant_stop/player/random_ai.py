from random import randrange

from cant_stop.player.player import Player


class RandomAiPlayer(Player):
    def __init__(self):
        super().__init__()
        self.name = "randomAI_%s" % str(randrange(1, 43))

    def choose_move(self, dice_roll, choices):
        possible_moves_list = [
            move for pair in choices for move in choices[pair]
        ]
        return possible_moves_list[randrange(len(possible_moves_list))]

    def build_an_anchor(self):
        return True if randrange(2) == 0 else False

    def burst(self, dice_roll, dice_pairs):
        pass
