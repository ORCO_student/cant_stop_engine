from random import randrange


class Dice():
    def __init__(self):
        pass

    def roll(self):
        return (
            randrange(1, 7), randrange(1, 7),
            randrange(1, 7), randrange(1, 7))

    def get_roll_and_pairs(self):
        dice_roll = a, b, c, d = tuple(self.roll())
        dice_pairs = {
            tuple(sorted((a+b, c+d))),
            tuple(sorted((a+c, b+d))),
            tuple(sorted((a+d, b+c)))
        }
        return dice_roll, dice_pairs
