from cant_stop.dice import Dice


COLUMNS = range(2, 13)
COLUMN_SIZES = {
    2: 2, 3: 4, 4: 6, 5: 8, 6: 10, 7: 11, 8: 10, 9: 8, 10: 6, 11: 4, 12: 2
}
MOVE = "MOVE"
DOUBLEMOVE = "DOUBLEMOVE"
ADD = "ADD"
DOUBLEADD = "DOUBLEADD"


class Game():
    def __init__(self, players_list):
        """
        Constructor
        Args:
            players_list: list of players objects
        """
        self.players = players_list
        self.markers = dict()    # neutral markers
        self.p_markers = {       # players' markers
            p: {col: 0 for col in COLUMNS} for p in range(len(self.players))
        }
        self.plays = 0           # current player turn
        self.dice = Dice()
        self.playable_columns = {col for col in COLUMNS}
        self.filled_columns = set()  # columns filled
        self.scores = dict()  # players' scores

    def play_game(self):
        """
        Method playing a full game.
        Returns:
            itself
        """
        self.init_scores()
        while all([score < 3 for score in self.scores.values()]):
            self.play_turn()  # play a turn
            self.plays = (self.plays+1) % len(self.players)  # next player
        return self

    def init_scores(self):
        self.scores.clear()
        self.scores = {p: 0 for p in range(len(self.players))}

    def play_turn(self):
        """
        This method plays one full turn for a player
        """
        player = self.players[self.plays]
        self.init_neutral_markers()
        while True:
            self.playable_columns = set(COLUMNS) - self.filled_columns
            dice_roll, dice_pairs = self.dice.get_roll_and_pairs()
            choices = self.get_possible_moves_with(dice_pairs)
            if self.player_can_move(choices):
                player_choice = player.choose_move(dice_roll, choices)
                self.execute_move(player_choice)

                if player.build_an_anchor():
                    self.place_player_markers()
                    break
            else:
                player.burst(dice_roll, dice_pairs)
                break

    def init_neutral_markers(self):
        """
        re-initialize neutral markers at the start of a turn
        """
        self.markers.clear()
        self.unused_markers = 3

    def get_possible_moves_with(self, pairs):
        """
        Returns all the possible moves depending
        on the current game state and on the dice result
        Args:
            pairs: a set of tuples of 2 ints
        Returns:
            a dict(pair: [move]) where:
                pair is a tuple of 2 ints
                [move] is a list of dict(column: action) where:
                    column is an int
                    action is a string of {MOVE, DOUBLEMOVE, ADD, DOUBLEADD}
        """
        result = dict()

        # active column <- columns with a neutral marker
        active_columns = {col for col in self.markers} & self.playable_columns

        for pair in pairs:
            result[pair] = []
            c1, c2 = pair

            # case 1: c1 == c2
            if c1 == c2:
                if c1 in active_columns:
                    result[pair] += [{c1: DOUBLEMOVE}]
                elif self.unused_markers > 0 and c1 in self.playable_columns:
                    result[pair] += [{c1: DOUBLEADD}]

            # case 2: neutral markers on c1 and on c2
            elif c1 in active_columns and c2 in active_columns:
                result[pair] += [{c1: MOVE, c2: MOVE}]
                # can eventually add dumb moves
                # result[pair] += [{c1: MOVE}, {C2: MOVE}]

            # case 3: neutral marker only on c1
            elif c1 in active_columns:
                if self.unused_markers > 0 and c2 in self.playable_columns:
                    result[pair] += [{c2: ADD}, {c1: MOVE, c2: ADD}]
                else:
                    result[pair] += [{c1: MOVE}]

            # case 4: neutral marker only on c2
            elif c2 in active_columns:
                if self.unused_markers > 0 and c1 in self.playable_columns:
                    result[pair] += [{c1: ADD}, {c2: MOVE, c1: ADD}]
                else:
                    result[pair] += [{c2: MOVE}]

            # case 5: c1 and c2 on columns with no markers
            else:
                if self.unused_markers > 0:
                    if c1 in self.playable_columns \
                       and c2 in self.playable_columns:
                        if self.unused_markers > 1:
                            result[pair] += [{c1: ADD, c2: ADD}]
                        else:
                            result[pair] += [{c1: ADD}, {c2: ADD}]
                    elif c1 in self.playable_columns:
                        result[pair] += [{c1: ADD}]
                    elif c2 in self.playable_columns:
                        result[pair] += [{c2: ADD}]

        return result

    def player_can_move(self, choices):
        return any([len(choices[pair]) > 0 for pair in choices])

    def execute_move(self, player_choice):
        """
        Executes the move chosen by the player
        """
        player_id = self.plays
        for column, action in player_choice.items():
            if action == ADD:
                self.markers[column] = self.p_markers[player_id][column] + 1
                self.unused_markers -= 1
            elif action == DOUBLEADD:
                self.markers[column] = self.p_markers[player_id][column] + 2
                self.unused_markers -= 1
            elif action == MOVE:
                self.markers[column] += 1
            elif action == DOUBLEMOVE:
                self.markers[column] += 2
            if self.markers[column] >= COLUMN_SIZES[column]:
                self.playable_columns.remove(column)

    def place_player_markers(self):
        """
        Places the player's anchors at the end of its turn
        """
        player_id = self.plays
        for column, position in self.markers.items():
            self.p_markers[player_id][column] = position
            if COLUMN_SIZES[column] <= position:
                self.scores[player_id] += 1
                self.filled_columns.add(column)
