from cant_stop.game.game import Game, COLUMNS, COLUMN_SIZES


class HumanGame(Game):
    def __init__(self, players):
        super().__init__(players)

    def play_turn(self):
        """
        play_turn overloaded to print some infos during the game
        """
        self.print_current_scores()
        p_name = self.players[self.plays].name
        print("%s turn is starting !" % p_name)
        super().play_turn()
        print("%s turn is over !" % p_name)
        print("_"*50 + "\n" + "_"*50)

    def print_current_scores(self):
        print(" ** Current scores: **")
        for player_id, player in enumerate(self.players):
            print('{:^15}'.format(player.name) + str(self.scores[player_id]))
        print(" **                 **\n")

    def print_ladders(self, player):
        """
        Prints a pseudo graphical output representing the player's ladders
        Args:
            player: player object
        """
        print("Current ladders for %s" % player.name)
        height = max(COLUMN_SIZES.values())
        for h in range(height):
            line = ""
            for col in COLUMNS:
                if COLUMN_SIZES[col] < height-h:
                    char = ' '
                elif col in self.markers \
                     and self.markers[col] == max(COLUMN_SIZES.values())-h:
                    char = '%'
                elif self.p_markers[self.plays][col] == max(COLUMN_SIZES.values())-h:
                    char = "*"
                elif col in self.filled_columns:
                    char = '/'
                else:
                    char = '|'
                line += "  %s  " % char
            print(line)
        line = "  "
        line += "   ".join(['{:^2}'.format(str(c)) for c in COLUMNS])
        print(line)
