#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from cant_stop.game.human_game import HumanGame
from cant_stop.player.human_player import HumanPlayer
from cant_stop.player.random_ai import RandomAiPlayer


def main(nb_human_players, nb_ai_players):

    players_list = []
    for i in range(nb_human_players):
        players_list += [
            HumanPlayer(input("Player %s name : \n" % (str(i+1))))
        ]
    for i in range(nb_ai_players):
        players_list += [RandomAiPlayer()]
    print("")
    game = HumanGame(players_list)
    for player in players_list:
        player.game = game
    game.play_game()


if __name__ == "__main__":
    try:
        nb_human_players = int(sys.argv[1])
        nb_ai_players = int(sys.argv[2])
    except Exception as _:
        print("Usage: %s nb_human_players nb_ai_players.")
        exit()
    main(nb_human_players, nb_ai_players)
